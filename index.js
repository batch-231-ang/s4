console.log("Hello World");

class Student{
	constructor(name, email, grades){
		this.name = name;
		this.email = email;

		if(grades.length === 4){
			if(grades.every(grade => grade >= 0 && grade <= 100)){
				this.grades = grades;
			}else{
				this.grades = undefined;
			}
		}else{
				this.grades = undefined;
		}

		this.gradeAve = undefined;
		this.passed = undefined;
		this.passedWithHonors = undefined;
	}

	login(){
		console.log(`${this.email} has logges in`);
		return this;
	}

	logout(email){
	    console.log(`${this.email} has logged out`);
	    return this;
	}

	listGrades(grades){
   	 this.grades.forEach(grade => {
        console.log(`${this.name}'s quarterly grade averages are: ${grade}`);
    	})
   	 return this;
	}

	computeAve(){
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		this.gradeAve = sum / 4;
		return this;
	}

	willPass(){
		// you can call methods inside an object
		this.passed = this.gradeAve >= 85 ? true : false;
		return this;
	}

	willPassWithHonors(){
		if(this.gradeAve >= 90){
			this.passedWithHonors = true;
		}else{
			this.passedWithHonors = false;
		}
		return this;
	}

}

/*let studentOne = new Student("Tony", "starksindustries@mail.com", [-1, 84, 78, 88]);
let studentTwo = new Student("Peter", "spideyman@mail.com", [78, 82, 79, 85]);
let studentThree = new Student("Wanda", "scarlettMaximoff@mail.com", [87, 89, 91, 93]);
let studentFour = new Student("Steve", "captainRogers@mail.com", [91, 89, 92, 93]);
*/

class Section{
	constructor(name){
		this.name = name;
		this.students = [];
		this.honorStudents = undefined;
		this.honorsPercentage = undefined;
	}

	addStudent(name, email, grades){
		this.students.push(new Student(name, email, grades));
		return this;
	}

	countHonorStudents(){
		let count = 0;
		this.students.forEach(student => {
			if(student.computeAve().willPass().willPassWithHonors().passedWithHonors){
				count++;
			}
		});
		this.honorStudents = count;
		return this;
	}

	computeHonorsPercentage(){
		this.honorsPercentage = (this.honorStudents / this.students.length) * 100;
		return this;
	}

}

// const section1A = new Section("section1A");
// console.log(section1A);


//mini-activity
class Grade{
	constructor(level){
		if(typeof level == "number"){
			this.level = level;
		}else{
			this.level = undefined;
		}

		this.sections = [];
		this.totalStudents = 0;
		this.totalHonorStudents = 0;
		this.batchAveGrade = undefined;
		this.batchMinGrade = undefined;
		this.batchMaxGrade = undefined;
	}

	addSection(name){
		this.sections.push(new Section(name));
	}

	countStudents(){
		this.sections.forEach(section => this.totalStudents = this.totalStudents + section.students.length);
		return this;
	}

	countHonorStudents(){
		let count = 0;
		this.sections.forEach(section => {
			section.students.forEach(student => {
				if(student.computeAve().willPass().willPassWithHonors().passedWithHonors){
					count++;
				}
			})
		});
		this.totalHonorStudents = count;
		return this;
	}

	/*
		Define a computeBatchAve() method that will get the average of all the students' grade averages and divide it by the total number of students in the grade level. The batchAveGrade property will be updated with the result of this operation.
	*/
	computeBatchAve(){
		let ave = 0;
		this.sections.forEach(section => {
			section.students.forEach(student => {
				ave += student.computeAve().gradeAve;
			})
		});
		ave /= this.countStudents().totalStudents;
		this.batchAveGrade = ave;
		return this;
	}

	/*
		Define a method named getBatchMinGrade() that will update the batchMinGrade property with the lowest grade scored by a student of this grade level regardless of section.
	*/
	getBatchMinGrade(){
		let listOfGrades = [];
		this.sections.forEach(section => {
			section.students.forEach(student => {
				student.grades.forEach(grade => {
					listOfGrades.push(grade);
				})
			})
		});
		this.batchMinGrade = listOfGrades.sort((a,b) => a-b)[0];
		return this;
	}

	/*
		Define a method named getBatchMaxGrade() that will update the batchMaxGrade property with the highest grade scored by a student of this grade level regardless of section.
	*/
	getBatchMaxGrade(){
		let listOfGrades = [];
		this.sections.forEach(section => {
			section.students.forEach(student => {
				student.grades.forEach(grade => {
					listOfGrades.push(grade);
				})
			})
		});
		this.batchMaxGrade = listOfGrades.sort((a,b) => b-a)[0];
		return this;
	}
}

let grade1 = new Grade(1);
console.log(grade1);

grade1.addSection("section1A");
grade1.addSection("section1B");
grade1.addSection("section1C");
grade1.addSection("section1D");

const section1A = grade1.sections.find(section => section.name === "section1A");
const section1B = grade1.sections.find(section => section.name === "section1B");
const section1C = grade1.sections.find(section => section.name === "section1C");
const section1D = grade1.sections.find(section => section.name === "section1D");

section1A.addStudent("Tony", "starksindustries@mail.com", [89, 84, 78, 88]);
section1A.addStudent("Peter", "spideyman@mail.com", [78, 82, 79, 85]);
section1A.addStudent("Wanda", "scarlettMaximoff@mail.com", [87, 89, 91, 93]);
section1A.addStudent("Steve", "captainRogers@mail.com", [91, 89, 92, 93]);

section1B.addStudent('Jeremy', 'jeremy@mail.com', [85, 82, 83, 89]);
section1B.addStudent('Johnny', 'johnny@mail.com', [82, 86, 77, 88]);
section1B.addStudent('Jerome', 'jerome@mail.com', [89, 85, 92, 91]);
section1B.addStudent('Janine', 'janine@mail.com', [90, 87, 94, 91]);

section1C.addStudent('Faith', 'faith@mail.com', [87, 85, 88, 91]);
section1C.addStudent('Hope', 'hope@mail.com', [85, 87, 84, 89]);
section1C.addStudent('Love', 'love@mail.com', [91, 87, 90, 88]);
section1C.addStudent('Joy', 'joy@mail.com', [92, 86, 90, 89]);

section1D.addStudent('Eddie', 'eddie@mail.com', [85, 87, 86, 92]);
section1D.addStudent('Ellen', 'ellen@mail.com', [88, 84, 86, 90]);
section1D.addStudent('Edgar', 'edgar@mail.com', [90, 89, 92, 86]);
section1D.addStudent('Eileen', 'eileen@mail.com', [90, 88, 93, 84]);